﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Model.IdentityModel
{
    public class ApplicationUserLogin : IdentityUserLogin<int>
    {
        public ApplicationUser User { get; set; }

        public override int UserId { get; set; }
    }
}
