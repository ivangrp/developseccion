﻿using AutoMapper;
using Seccion.Model.Models;
using Seccion.Service.Common.Message;
using Seccion.Service.ServiceInterface;
using Seccion.Web.Common;
using Seccion.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Seccion.Web.Controllers
{
    [Authorize]
    public class ExpensesController : Controller
    {
		private readonly INotifier notifier;
		private readonly IEmployeeService employeeService;

		public ExpensesController(INotifier notifier, IEmployeeService employeeService)
		{
			this.notifier = notifier;
			this.employeeService = employeeService;
		}
        // GET: Expenses
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<ActionResult> CreateExpenses()
        {
            var listEmployee = await employeeService.GetEmployeeAllForRequest();
            ViewBag.Employee = new SelectList(listEmployee.OrderBy(x => x.TbEmployeeId), "TbEmployeeId", "EmployeeNameComplet");
            ViewBag.PaymentType = ListsDrops.PaymentType();
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> CreateExpenses(ExpensesViewModel model)
        {
			try
			{
                var expenses = Mapper.Map<ExpensesViewModel, TbFuneralExpenses>(model);
                expenses.DateCreate = DateTime.Now;
                expenses.DateInsert = DateTime.Now;
                expenses.UserInsert = User.Identity.Name;
            }
			catch (Exception ex)
			{
                return View("Error", new HandleErrorInfo(ex, "ListExpenses", "Expenses"));
            }
            return View();
        }
    }
}