﻿using Seccion.Service.ServiceInterface;
using Seccion.Web.Models.GridListModel;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Seccion.Web.Controllers
{
    public class CommonController : Controller
    {
		private readonly IEmployeeService employeeService;

		public CommonController(IEmployeeService employeeService)
		{
			this.employeeService = employeeService;
		}
        // GET: Common
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult DataTableMenu(MenuIdModel model)
        {
            return PartialView("_TableMenu", model);
        }
        [HttpGet]
        public async Task<JsonResult> GetEmployeeAval()
        {
            var listEmployee = await employeeService.GetEmployeeData();


            return Json(listEmployee, JsonRequestBehavior.AllowGet);
        }
    }
}