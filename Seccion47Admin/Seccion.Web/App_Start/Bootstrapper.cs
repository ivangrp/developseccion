﻿using System;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using AutoMapper;
using Seccion.Data.Infrastructure;
using Seccion.Data.Repositories;
using Seccion.Service.Common.Message;
using Seccion.Service.Services;
using Seccion.Web.Mappings;

namespace Seccion.Web
{
    public class Bootstrapper
    {
        public static void Run()
        {
            SetAutofacContainer();
            AutoMapperConfiguration.Configure();
        }

        private static void SetAutofacContainer()
        {
            var builder = new ContainerBuilder();
            //builder.RegisterControllers(typeof(BaseLanguajeController).Assembly);
            var curenntAssembly = Assembly.GetExecutingAssembly();
            
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerRequest();
            //builder.RegisterType<ApplicationUserManager>().AsSelf().InstancePerRequest();
            //builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerRequest();
            builder.RegisterType<Notifier>().AsImplementedInterfaces().InstancePerRequest();
            builder.RegisterType<NotifierFilterAttribute>().AsActionFilterFor<Controller>().PropertiesAutowired();            

            // Repositories
            builder.RegisterAssemblyTypes(typeof(EmployeeRepository).Assembly).Where(t => t.Name.EndsWith("Repository")).AsImplementedInterfaces().InstancePerRequest();

            // Services
            builder.RegisterAssemblyTypes(typeof(EmployeeService).Assembly).Where(t => t.Name.EndsWith("Service")).AsImplementedInterfaces().InstancePerRequest();

            //var assemblyNames = Assembly.GetExecutingAssembly().GetReferencedAssemblies();
            //var assembliesTypes = assemblyNames
            //    .Where(a => a.Name.Equals("Seccion.Web.Mappings", StringComparison.OrdinalIgnoreCase))
            //    .SelectMany(an => Assembly.Load(an).GetTypes())
            //    .Where(p => typeof(Profile).IsAssignableFrom(p) && p.IsPublic && !p.IsAbstract)
            //    .Distinct();

            //var autoMapperProfiles = assembliesTypes
            //    .Select(p => (Profile)Activator.CreateInstance(p)).ToList();

            //builder.Register(ctx => new MapperConfiguration(cfg =>
            //{
            //    foreach (var profile in autoMapperProfiles)
            //    {
            //        cfg.AddProfile(profile);
            //    }
            //}));

            //builder.Register(ctx => ctx.Resolve<MapperConfiguration>().CreateMapper()).As<IMapper>().InstancePerLifetimeScope();
            builder.RegisterFilterProvider();
            builder.RegisterControllers(curenntAssembly);

            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}