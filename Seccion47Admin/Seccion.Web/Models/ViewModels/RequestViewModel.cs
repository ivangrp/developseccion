﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Seccion.Web.Models.ViewModels
{
	public class RequestViewModel
	{
        /// <summary>
        /// PK de la tabla
        /// </summary>
        public int TbRequestId { get; set; }
        /// <summary>
        /// FK con PK empleados
        /// </summary>
        [Display(Name = "Empleado Solicitante")]
        public int TbEmployeeId { get; set; }
        /// <summary>
        /// Fecha de creacion de la solicitud
        /// </summary>
        [Display(Name = "Fecha Creación")]
        public string DateCreateRequest { get; set; }
        /// <summary>
        /// Numero de la solicitud
        /// </summary>
        [Display(Name = "No Solicitud")]
        public string RequestNumber { get; set; }
        /// <summary>
        /// Monto a pagar
        /// </summary>
        [Display(Name = "Monto Total")]
        public string Amount { get; set; }
        /// <summary>
        /// Peridos de pago
        /// </summary>
        [Display(Name = "Periodo de Pagos")]
        public string PaymentsPeriod { get; set; }
        /// <summary>
        /// Tasa de descuento
        /// </summary>
        [Display(Name = "Tasa de Descuento")]
        public string DiscountRate { get; set; }
        /// <summary>
        /// Gastos de administracion
        /// </summary>
        [Display(Name = "% Gastos Administracion")]
        public string AdministrationExpenses { get; set; }
        /// <summary>
        /// Resultado de gastos de administracion
        /// </summary>
        [Display(Name = "Resultado Gastos Administracion")]
        public string AdministrationExpensesResult { get; set; }
        /// <summary>
        /// Meses adicionales
        /// </summary>
        [Display(Name = "Meses Adicionales")]
        public string AdditionalMonths { get; set; }
        /// <summary>
        /// Total a pagar
        /// </summary>
        [Display(Name = "Total a Pagar")]
        public string TotalPay { get; set; }
        /// <summary>
        /// Abono catorcenal o mensual
        /// </summary>
        [Display(Name = "Abono Catorcenal/Mensual")]
        public string AbonoCatorcenalMensual { get; set; }
        /// <summary>
        /// Catorcena a liquidar
        /// </summary>
        [Display(Name = "Catorcena a Liquidar")]
        public string CatorcenaLiquidate { get; set; }
        /// <summary>
        /// Seccion a la que pertenece
        /// </summary>
        [Display(Name = "Sección Perteneciente")] 
        public string BelongingSection { get; set; }
        /// <summary>
        /// Fecha de proxima subida
        /// </summary>
        [Display(Name = "Próxima Subida")]
        public string NextRise { get; set; }
        /// <summary>
        /// Nombre del Aval
        /// </summary>
        
        public string AvalName { get; set; }
        /// <summary>
        /// Clave del Aval
        /// </summary>
        [Display(Name = "Nombre Aval")]
        public string AvalKey { get; set; }
        /// <summary>
        /// Adeudo del Aval
        /// </summary>
        [Display(Name = "Adeudo Aval")]
        public string DebitAval { get; set; }
        /// <summary>
        /// Comentarios generales
        /// </summary>
        [Display(Name = "Observaciones")]
        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }
        /// <summary>
        /// Estatus de la solicitud
        /// </summary>

        public int Status { get; set; }

        public string DateInsert { get; set; }
        public string DateUpdate { get; set; }
        public string UserInsert { get; set; }
        public string UserUpdate { get; set; }
        public string Ficha { get; set; }
        public string FichaAval { get; set; }
        public int Location { get; set; }
        public string EmployeeName { get; set; }
    }
}