﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Seccion.Web.Models.ViewModels
{
	public class FuneralExpensesViewModel
	{
        public int FuneralExpensesID { get; set; }

        [Display(Name = "Empleado:")]
        public int EmployeeID { get; set; }

        [Display(Name = "Folio:")]
        public string OrderNumberGF { get; set; }

        [Display(Name = "Concepto Servicio:")]
        public string ServiceConcept { get; set; }

        [Display(Name = "Tipo de Pago:")]
        public string TipoPago { get; set; }

        [Display(Name = "Clave Boucher:")]
        public string VoucherKey { get; set; }

        [Display(Name = "Descontar a:")]
        public string PersonaDescontar { get; set; }

        [Display(Name = "Monto a Pagar:")]
        public decimal PaymentAmount { get; set; }

        [Display(Name = "Perido de Pago:")]
        public string PeriodsPayment { get; set; }

        [Display(Name = "Abono Catorcenal/Mensual:")]
        public decimal AbonoCatorcenalMensual { get; set; }

        [Display(Name = "Observaciones:")]
        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }

        public DateTime? DateInsert { get; set; }
        public DateTime? DateUpdate { get; set; }
        public string UserInsert { get; set; }
        public string UserUpdate { get; set; }
    }
}