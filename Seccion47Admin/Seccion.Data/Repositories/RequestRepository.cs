﻿using Seccion.Data.Infrastructure;
using Seccion.Data.ResposeDto;
using Seccion.Model.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Data.Repositories
{
	public interface IRequestRepository: IRepository<TbRequest>
	{
		string GetMaxRequest();
        Task<IEnumerable<SP_RequestDto>> GetListRequest(params object[] parameters);

    }
	public class RequestRepository : RepositoryBase<TbRequest>, IRequestRepository
	{
		public RequestRepository(IDbFactory dbFactory) : base(dbFactory)
		{
		}
		public string GetMaxRequest()
		{
			return DbContext.Request.Max(x => x.RequestNumber);
		}
        public async Task<IEnumerable<SP_RequestDto>> GetListRequest(params object[] parameters)
        {
            try
            {
                using (var connection = DbContext.Database.Connection)
                {
                    connection.Open();
                    var command = connection.CreateCommand();
                    //Agregar el SP a utilizar
                    command.CommandText = "[dbo].[SP_Get_Request]";//"[dbo].[SP_Employee_SelectAll]";
                    command.Parameters.AddRange(parameters);
                    command.CommandType = CommandType.StoredProcedure;
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        var result = ((IObjectContextAdapter)DbContext).ObjectContext.Translate<SP_RequestDto>(reader).ToList();
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
