﻿using Seccion.Data.InertazRepositories;
using Seccion.Data.Infrastructure;
using Seccion.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Data.Repositories
{
	public class ExpensesRepository : RepositoryBase<TbFuneralExpenses>, IExpensesRepository
	{
		public ExpensesRepository(IDbFactory dbFactory) : base(dbFactory)
		{
		}
	}
}
