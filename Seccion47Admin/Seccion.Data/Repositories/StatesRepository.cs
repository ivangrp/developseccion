﻿using Seccion.Data.Infrastructure;
using Seccion.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Data.Repositories
{
	public interface IStatesRepository : IRepository<TbStates>
	{

	}
	public class StatesRepository : RepositoryBase<TbStates>, IStatesRepository
	{
		public StatesRepository(IDbFactory dbFactory) : base(dbFactory)
		{
		}
	}
}
