﻿using Seccion.Data.Infrastructure;
using Seccion.Data.Enum;
using Seccion.Model.IdentityModel;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using System.Security.Claims;
using System.Collections.Generic;
using System;
using System.Linq;
using Microsoft.AspNet.Identity;

namespace Seccion.Data.Repositories
{
	/// <summary>
	/// Interfaz que implementa las funciones o metodos para las cuenta de usuario
	/// </summary>
	public interface IAccountUserRepository : IRepository<ApplicationUser>
	{
		Task<IdentityResult> AccountUserCreate(ApplicationUser user, string pass);
		Task<IEnumerable<ApplicationUser>> GetUserList();
		Task<ClaimsIdentity> CreateUserIdentity(ApplicationUser appUser);

		Task<IdentityResult> ChangePasswordUser(int userId, string currentPassword, string newPassword);
		Task<string> EmailConfirmationToken(ApplicationUser appUser);
		Task<ApplicationUser> GetUserByIdForLogin(string userNameOrEmail);
		Task<ApplicationUser> GetUserById(int id);
		Task<SignInStatusUser> SignInUser(string userName, string password);
		/// <summary>
		/// Obtiene
		/// </summary>
		/// <returns>Lisatdo de roles</returns>
		List<Tuple<int, string>> GetRoles();
		Task<IdentityResult> AddRole(int userId, string role);
		Task<string> GetRoleById(int roleId);
		Task<IList<string>> GetRoleByUserId(int userId);
		Task<IdentityResult> DeleteRoleByUserId(int userId, string role);
	}
	public class AccountUserRepository : RepositoryBase<ApplicationUser>, IAccountUserRepository
	{
		//private ApplicationSignInManager _signInManager;
		private ApplicationUserManager userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
		private ApplicationSignInManager _signInManager = HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
		private ApplicationRoleManager _AppRoleManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationRoleManager>();

		public AccountUserRepository(IDbFactory dbFactory) : base(dbFactory)
		{

		}

		public ApplicationUserManager UserManager { get => userManager; set => userManager = value; }
		public ApplicationSignInManager SignInManager { get => _signInManager; set => _signInManager = value; }
		public ApplicationRoleManager AppRoleManager { get => _AppRoleManager; set => _AppRoleManager = value; }


		public async Task<IdentityResult> AccountUserCreate(ApplicationUser user, string pass)
		{
			var result = await UserManager.CreateAsync(user, pass);

			return result;
		}
		public async Task<ApplicationUser> GetUserByIdForLogin(string userNameOrEmail)
        {
			return await DbContext.Users.FirstOrDefaultAsync(x => x.UserName == userNameOrEmail || x.Email == userNameOrEmail);
		}
		public async Task<ApplicationUser> GetUserById(int id)
		{
			return await DbContext.Users.Include(x => x.Roles).FirstOrDefaultAsync(x => x.Id == id);
		}
		public async Task< IEnumerable<ApplicationUser>> GetUserList()
		{
			return await UserManager.Users.ToListAsync();
		}
		public async Task<string> EmailConfirmationToken(ApplicationUser appUser)
		{
			return await UserManager.GenerateEmailConfirmationTokenAsync(appUser.Id);
		}
		public async Task<SignInStatusUser> SignInUser(string userName, string password)
		{
			var result = await SignInManager.PasswordSignInAsync(userName, password, isPersistent: false, shouldLockout: false);
			return result == SignInStatus.Success ? SignInStatusUser.Success : result == SignInStatus.LockedOut ? SignInStatusUser.LockedOut : result == SignInStatus.RequiresVerification ? SignInStatusUser.RequiresVerification : SignInStatusUser.Failure;
		}
		public async Task<ClaimsIdentity> CreateUserIdentity(ApplicationUser appUser)
		{
			return await SignInManager.CreateUserIdentityAsync(appUser);
		}
		public async Task<IdentityResult> ChangePasswordUser(int userId, string currentPassword, string newPassword)
		{
			var result = await UserManager.ChangePasswordAsync(userId, currentPassword, newPassword);

			return result;
		}
		/// <summary>
		/// Obtiene
		/// </summary>
		/// <returns>Lisatdo de roles</returns>
		public List<Tuple<int, string>> GetRoles()
		{

			var listRoles = AppRoleManager.Roles.ToList().Select(x => new Tuple<int, string>(x.Id, x.Name)).ToList();
			return listRoles;
		}
		public async Task<IdentityResult> AddRole(int userId, string role)
		{
			return await userManager.AddToRoleAsync(userId, role);
		}
		public async Task<string> GetRoleById(int roleId)
		{
			var result = await AppRoleManager.FindByIdAsync(roleId);
			return result.Name;
		}
		public async Task<IList<string>> GetRoleByUserId(int userId)
		{
			//var result = await AppRoleManager.Roles.Where(e => e.Users.Any(y => y.UserId == userId)).ToListAsync();
			var result = await UserManager.GetRolesAsync(userId);
			return result;
		}
		public async Task<IdentityResult> DeleteRoleByUserId(int userId, string role)
		{
			var result = await UserManager.RemoveFromRoleAsync(userId, role);
			return result;
		}
	}
}
