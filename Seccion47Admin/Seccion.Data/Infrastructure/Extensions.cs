﻿using Seccion.Model.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;


namespace Seccion.Data.Infrastructure
{
    public static class Extensions
    {
        /// <summary>
        /// Obtiene el ID de Empresa (RENAULT, NISSAN) para usarlo con User Identity
        /// </summary>
        /// <param name="identity">Identity</param>
        /// <returns>Retorna el Guid de la empresa</returns>
        //public static TcUserTypes GetUserType(this IIdentity identity)
        //{
        //    StoreEntities db = new StoreEntities();
        //    TcUserTypes type = new TcUserTypes();
        //    var UserID = identity.GetUserId();
        //    var usuario = db.Users.Where(x=>x.Id== UserID).FirstOrDefault();
        //    type = db.UserTypes.Where(u => u.UserTypeID == usuario.UserTypeID).FirstOrDefault();
        //    return type;
        //}

        public static string GetName(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("Nombre");
            return (claim != null) ? claim.Value : "Usuario";
        }

        public static String GetUserType(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("UserTypeID");
            return claim == null ? "" : claim.Value;
        }
        public static String GetRole(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("Rol");
            return claim.Value;
        }
        public static String GetRank(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("RankUser");
            return (claim != null) ? claim.Value : "";
        }
        public static String GetRankID(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("RankID");
            return (claim != null) ? claim.Value : "";
        }
        public static string GetRankGroup(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("RankGroupUser");
            return (claim != null) ? claim.Value : "";
        }
        //public static string GetCompany(this IIdentity identity)
        //{
            //StoreEntities db = new StoreEntities();
            //if (identity.GetUserType() != string.Empty)
            //{
            //    var userID = Guid.Parse(identity.GetUserType());
            //    var User = db.UserTypes.Where(x => x.UserTypeID == userID).SingleOrDefault();
            //    return User.Description;
            //}
            //else
            //{
            //    return string.Empty;
            //}
        //}

        public static int GetRolId(this IIdentity identity)
        {
            StoreEntities db = new StoreEntities();
            
            var userID = identity.GetRole();
            var User = db.Roles.Where(x => x.Name == userID).SingleOrDefault();
            return User.Id;
        }

        //public static string GetUserPhoto(this IIdentity identity)
        //{
        //    try
        //    {
        //        ApplicationDbContext db = new ApplicationDbContext();
        //        var userID = identity.GetUserId();
        //        var User = db.Users.Where(x => x.Id == userID).First();
        //        return (User.UserPhoto != null) ? "/../uploads/profilePictures/" + User.UserPhoto : "/Content/images/man.png";
        //    }
        //    catch
        //    {
        //        return "";
        //    }
        //}

        //public class CustomReportCredentials : IReportServerCredentials
        //{
        //    private string _UserName;
        //    private string _PassWord;
        //    private string _DomainName;

        //    public CustomReportCredentials(String UserName, String Password, String Dominio)
        //    {
        //        _UserName = UserName;
        //        _PassWord = Password;
        //        _DomainName = Dominio;
        //    }


        //    public bool GetFormsCredentials(out System.Net.Cookie authCookie, out string userName, out string password, out string authority)
        //    {
        //        authCookie = null;
        //        userName = password = authority = null;
        //        return false;
        //    }

        //    public System.Security.Principal.WindowsIdentity ImpersonationUser
        //    {
        //        get { return null; }
        //    }

        //    public System.Net.ICredentials NetworkCredentials
        //    {
        //        get { return new NetworkCredential(_UserName, _PassWord, _DomainName); }
        //    }
        //}
    }
}
