﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Data.ResposeDto
{
	public class EmployeeDto
	{
		public int EmployeeId { get; set; }
		public string NumberFicha {get; set;}
		public string ContractualSituation { get; set;}
		public string WorkLocation { get; set;}
		public string NameEmployee { get; set; }
		public string FirstName{ get; set; }
	    public string LastName{ get; set; }

	}
}
