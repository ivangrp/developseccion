﻿using Seccion.Data.Infrastructure;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seccion.Data
{
    public class StoreSeedData: DropCreateDatabaseIfModelChanges<StoreEntities>
    {
        protected override  void Seed(StoreEntities context)
        {
            //GetCategories().ForEach(c => context.Categories.Add(c));
            //GetGadgets().ForEach(g => context.Gadgets.Add(g));
            //base.Seed(context);

            try
            {
                //ApplicationRoleManager _roleManager = new ApplicationRoleManager(new RoleStore<IdentityRole>(context));

                //if (!await _roleManager.RoleExistsAsync("Administrador"))
                //    await _roleManager.CreateAsync(new IdentityRole() { Name = "Administrador" });
                //if (!await _roleManager.RoleExistsAsync("Usuário"))
                //    await _roleManager.CreateAsync(new IdentityRole() { Id = "17EDA239-8F80-4B5A-A59F-E2D55A26DFBB", Name = "Usuário" });
                //if (!await _roleManager.RoleExistsAsync("Frota"))
                //    await _roleManager.CreateAsync(new IdentityRole() { Name = "Frota" });
                //if (!await _roleManager.RoleExistsAsync("RH"))
                //    await _roleManager.CreateAsync(new IdentityRole() { Name = "RH" });
                //if (!await _roleManager.RoleExistsAsync("Contabilidade"))
                //    await _roleManager.CreateAsync(new IdentityRole() { Name = "Contabilidade" });
            }
            catch { }
            context.Commit();
        }
 
    }
}
